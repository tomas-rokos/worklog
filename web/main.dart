import "package:angular2/platform/browser.dart";
import "package:worklog/layout_component.dart";

import 'package:firebase/firebase.dart' as firebase;

void main() {
	firebase.initializeApp(
		apiKey: "AIzaSyC7vjfiFevgwynNhvKigfS9gTYUS7PfXkk",
		authDomain: "worklog-72eaf.firebaseapp.com",
		databaseURL: "https://worklog-72eaf.firebaseio.com",
//		projectId: "worklog-72eaf",
		storageBucket: "worklog-72eaf.appspot.com",
//		messagingSenderId: "509869458141"
	);

	bootstrap(LayoutComponent);
}