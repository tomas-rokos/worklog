// Copyright (c) 2017, Tomáš Rokos. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/core.dart';

@Component(
	selector: '[clock-elm]',
	styleUrls: const ['clock_component.css'],
	templateUrl: 'clock_component.html',
	directives: const [],
	providers: const [],
	)
class ClockComponent {
}
