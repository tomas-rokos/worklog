import "package:angular2/angular2.dart";

import "clock_component.dart";

@Component (
	selector: "worklog-layout",
	templateUrl: "layout_component.html",
	styleUrls: const ["layout_component.css"],
	directives: const [ClockComponent],
	providers: const []
)
class LayoutComponent {

}